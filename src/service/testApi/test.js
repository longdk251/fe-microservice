import request from '../index'

export function loadapiuser() {
  return request({
    url: '/api/test/user',
    method: 'get'
  })
}
export function loadapiadmin() {
  return request({
    url: '/api/test/admin',
    method: 'get'
  })
}
export function loadapimod() {
  return request({
    url: '/api/test/moad',
    method: 'get'
  })
}