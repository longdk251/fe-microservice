import axios from 'axios';
import authHeader from './login/auth-header';
const service  = axios.create({
    timeout: 20000, 
    headers : authHeader() 
})

export default service