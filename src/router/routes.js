const routes = [
  { path: "/", component: () => import("@/views/login.vue") },
  { path: "/login", component: () => import("@/views/login.vue") },
  {
    path: "/admin",
    component: () => import("@/layout/LayoutAdmin.vue"),
    children: [
        { path: "home", component: () => import("@/views/Home.vue") },
        { path: "about", component: () => import("@/views/About.vue") },
        { path: "phanquyen", component: () => import("@/views/PhanQuyen.vue") },
        { path: "danhsachgdtdt", component: () => import("@/views/PageGD.vue") },
        { path: "createuser", component: () => import("@/views/CreateUser.vue") },
        { path: "createdoitac", component: () => import("@/views/CreateDoiTac.vue") }
    ],
  },
];
// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
      path: '/:catchAll(.*)',
      component: () =>
          import ('../views/Error404.vue')
  })
}
export default routes;
