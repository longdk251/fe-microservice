import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index.js";
import store from "./store";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import JQuery from 'jquery';
window.$ = JQuery;
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import './assets/css/style.css';

createApp(App).use(store).use(router).mount("#app");
