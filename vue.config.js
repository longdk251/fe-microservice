module.exports = {
  devServer: {
    port: 8085,
    open: true,
    proxy: {
      "/api": {
        target: process.env.VUE_APP_BASE_API,
        changeOrigin: true,
        ws: true,
      },
    }
  },
};
